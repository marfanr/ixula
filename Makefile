include build.conf

default:
	mkdir -p $(BUILDDIR)
	@make -C $(BOOTDIR)
	mkdir -p $(BUILDDIR)/iso/boot/grub
	cp ./grub.cfg $(BUILDDIR)/iso/boot/grub/grub.cfg
	cp $(BUILDDIR)/main.bin $(BUILDDIR)/iso/boot/main.bin
	grub-mkrescue -o $(BUILDDIR)/ixula.iso $(BUILDDIR)/iso > /dev/null 2>&1

run:
	$(QEMU) -cdrom $(BUILDDIR)/ixula.iso --enable-kvm
