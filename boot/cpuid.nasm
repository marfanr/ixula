global cpuidcheck
cpuidcheck:
pushfd
pop eax
mov ecx, eax
xor eax, 1 << 21
push eax
popfd
pushfd
pop eax
push ecx
popfd
xor eax, ecx
jz .nocpuid
ret

.nocpuid:
mov byte [0xb8000], 'c'
hlt

