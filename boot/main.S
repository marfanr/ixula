#define MAGIC 0xE85250D6 // multiboot2 
#define FLAGS -(MAGIC + 0 + (heaader_end - header_start))
 
.section .multiboot2
_beginning:

header_start:
.long MAGIC, 0
.long heaader_end - header_start
.long FLAGS

// tag begin
entry:
.align 8
.word 3, 1
.long entry_end - entry
.long _start
entry_end:

// framebuffer
framebuffer:
// .align 8
// .word 5, 1
// .long framebuffer_end - framebuffer
// .long 0
// .long 0
// .long 32
framebuffer_end:

// end of tags
.align 8
.word 0, 0
.long 8
heaader_end:

.section .bss
.align 16
stack_bottom:
.skip 16384 
stack_top:

.section .data
// Global Descriptor Table
gdt:
   .8byte 0x0  // null
   .8byte 0x0  // null



.section .text
.type _start, @function
.global _start
.code32
_start:
    mov $stack_top, %esp
    pushl $0
    popf
    call cpuidcheck
    pushl %ebx
    pushl %eax
    call _init    

    cli
1: hlt
   jmp 1b

